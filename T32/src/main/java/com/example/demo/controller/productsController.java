package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.products;
import com.example.demo.service.productsImpl;

@RestController
@RequestMapping("/api")
public class productsController {

	@Autowired
	productsImpl productsImpl;
	
	@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
	
	@GetMapping("/products")
	public List<products> listarproducts(){
		return productsImpl.listarProducts();
	}
	
	@PostMapping("/products")
	public products guardarproducts(@RequestBody products products) {
		
		return productsImpl.guardarProducts(products);
	}
	
	@GetMapping("/products/{id}")
	public products productsXID(@PathVariable(name="id") int id) {
		
		products products_xid= new products();
		
		products_xid=productsImpl.productsXID(id);
		
		System.out.println("products XID: "+products_xid);
		
		return products_xid;
	}
	
	@PutMapping("/products/{id}")
	public products actualizarproducts(@PathVariable(name="id")int id,@RequestBody products products) {
		
		products products_seleccionado= new products();
		products products_actualizado= new products();
		
		products_seleccionado= productsImpl.productsXID(id);
		
		products_seleccionado.setName(products.getName());
		products_seleccionado.setDetail(products.getDetail());
        products_seleccionado.setCreated_at(products.getCreated_at());
        products_seleccionado.setUpdated_at(products.getUpdated_at());

		
		products_actualizado = productsImpl.actualizarProducts(products_seleccionado);
		
		System.out.println("El products actualizado es: "+ products_actualizado);
		
		return products_actualizado;
	}
	
	@DeleteMapping("/products/{id}")
	public void eliminarproducts(@PathVariable(name="id")int id) {
		productsImpl.eliminarProducts(id);
	}
}

