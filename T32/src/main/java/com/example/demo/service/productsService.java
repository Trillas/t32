package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.products;

public interface productsService {
	public List<products> listarProducts(); //Listar All 
	
	public products guardarProducts(products products);	//Guarda un products CREATE
	
	public products productsXID(int id); //Leer datos de un products READ
	
	public products actualizarProducts(products products); //Actualiza datos del products UPDATE
	
	public void eliminarProducts(int id);// Elimina el products DELETE
}
