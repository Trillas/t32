package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.productsDao;
import com.example.demo.dto.products;

@Service
public class productsImpl implements productsService{
	
	@Autowired
	productsDao productsDao;
	
	@Override
	public List<products> listarProducts() {
		return productsDao.findAll();
	}

	@Override
	public products guardarProducts(products products) {
		return productsDao.save(products);
	}

	@Override
	public products productsXID(int id) {
		return productsDao.findById(id).get();
	}

	@Override
	public products actualizarProducts(products products) {
		return productsDao.save(products);
	}

	@Override
	public void eliminarProducts(int id) {
		productsDao.deleteById(id);
		
	}	
}
