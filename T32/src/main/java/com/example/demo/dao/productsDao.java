package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.dto.products;

public interface productsDao extends JpaRepository<products, Integer> {

}
